import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AzulComponent } from "./azul/azul.component";
import { VerdeComponent } from "./verde/verde.component";
import { ColorComponent } from "./color/color.component";
import { NotFoundComponent } from "./not-found/not-found.component";

const routes: Routes = [
  { path: "dame/el/azul", component: AzulComponent },
  { path: "verde", component: VerdeComponent },
  {
    path: "color/:color",
    component: ColorComponent,
    data: {a : "dato 1", b: "dato 2"}
  },

  { path: "**", component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
