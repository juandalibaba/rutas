import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-color",
  templateUrl: "./color.component.html",
  styleUrls: ["./color.component.css"]
})
export class ColorComponent implements OnInit {
  color: string;
  data: {};
  queryData: {};

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((c: any) => {
      console.log(c);
      this.color = c.params.color;
    })

    this.activatedRoute.data.subscribe(d => {
      console.log(d);
      this.data = d;
    })

    this.activatedRoute.queryParams.subscribe( q => {
      console.log(q)
      this.queryData = q
    })
  }

  goToColor(color: String) {
    this.router.navigate([color]);
  }
}
